<?php

namespace WebLinks\DAO;

use WebLinks\Domain\Link;

class LinkDAO extends DAO 
{
    /**
     * @var \WebLinks\Domain\UserDAO
     */
    private $userDAO;

    public function setUserDAO(UserDAO $userDAO) {
        $this->userDAO = $userDAO;
    }

    /**
     * Returns a list of all links, sorted by id.
     *
     * @return array A list of all links.
     */
    public function findAll() {
        $sql = "select * from t_link order by link_id desc";
        $result = $this->getDb()->fetchAll($sql);
        
        // Convert query result to an array of domain objects
        $links = array();
        foreach ($result as $row) {
            $linkId = $row['link_id'];
            $links[$linkId] = $this->buildDomainObject($row);
        }
        return $links;
    }

    /**
     * Returns a link matching the supplied id
     * @param  integer $id The link id
     * @return \WebLinks\Domain\Link|throws an exeption if no mathing link
     */
    public function find($id){
        $sql = "select * from t_link where link_id=?";
        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row) {
            return $this->buildDomainObject($row);
        } else {
            throw new \Exception("No link matching id ".$id);
        }
    }

    /**
     * Removes a link from the database
     * @param  Integer $id The link id
     */
    public function delete($id){
        $this->getDb()->delete('t_link', array('link_id' => $id));
    }

    public function deleteAllByUser($userId){
        $this->getDb()->delete('t_link', array('user_id' => $userId));
    }

    /**
     * Save a link in the database
     * @param  \WebLinks\Domain\Link   $link The link to save
     */
    public function save(Link $link) {
        $linkData = array(
            'link_title' => $link->getTitle(),
            'link_url' => $link->getUrl(),
            'user_id' => $link->getUser()->getId()
            );

        if ($link->getId()) {
            // Link has already been saved, we need to update it
            $this->getDb()->update('t_link', $linkData, array('link_id' => $link->getId()));
        } else {
            // Add the link to the data base
            $this->getDb()->insert('t_link', $linkData);
            // Set the new id and set it on the entity
            $id = $this->getDb()->lastInsertId();
            $link->setId($id);
        }
    }

    /**
     * Creates an Link object based on a DB row.
     *
     * @param array $row The DB row containing Link data.
     * @return \WebLinks\Domain\Link
     */
    protected function buildDomainObject($row) {
        $link = new Link();
        $link->setId($row['link_id']);
        $link->setUrl($row['link_url']);
        $link->setTitle($row['link_title']);

        if(array_key_exists('user_id', $row)) {
            // Find associated user
            $userId = $row['user_id'];
            $user = $this->userDAO->find($userId);
            $link->setUser($user);
        }
        
        return $link;
    }
}
