<?php

namespace WebLinks\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use WebLinks\Domain\User;
use WebLinks\Form\Type\UserType;
use WebLinks\Form\Type\LinkType;

class AdminController {

	/**
	 * Admin home page controller
	 * @param  Application $app Silex application
	 * @return view             HTML view of the admin page
	 */
	public function indexAction(Application $app) {
		$users = $app['dao.user']->findAll();
		$links = $app['dao.link']->findAll();

		return $app['twig']->render('admin.html.twig', array(
			'users' => $users,
			'links' => $links
		));
	}

	/**
	 * Admin add user form page controller
	 * @param Application $app     Silex application
	 * @param Request     $request Incoming request
	 */
	public function addUserAction(Application $app, Request $request) {
		$user = new User();
		$userForm = $app['form.factory']->create(new UserType(), $user);
		$userForm->handleRequest($request);
		if($userForm->isSubmitted() && $userForm->isValid()){
			// Generate a random salt value to set on the user
			$salt = substr(md5(time()), 0, 23);
			$user->setSalt($salt);
			$plainPassword = $user->getPassword();
			// Load default encoder and encode password
			$encoder = $app['security.encoder.digest'];
			$password = $encoder->encodePassword($plainPassword, $user->getSalt());
			$user->setPassword($password);
			$app['dao.user']->save($user);
			$app['session']->getFlashBag()->add('success', 'User has been successfully added to the database.');
		}
		return $app['twig']->render('user_form.html.twig', array(
			'title' => 'New user',
			'userForm' => $userForm->createView()));
	}

	/**
	 * Admin edit user form page controller
	 * @param Application $app     Silex application
	 * @param Request     $request Incoming request
	 * @param  integer $id User id
	 * @return view             HTML view of the admin page
	 */
	public function editUserAction(Application $app, Request $request, $id) {
		$user = $app['dao.user']->find($id);
		$userForm = $app['form.factory']->create(new UserType(), $user);
		$userForm->handleRequest($request);
		if($userForm->isSubmitted() && $userForm->isValid()){
			$plainPassword = $user->getPassword();
			// Load default encoder and encode password
			$encoder = $app['security.encoder.digest'];
			$password = $encoder->encodePassword($plainPassword, $user->getSalt());
			$user->setPassword($password);
			$app['dao.user']->save($user);
			$app['session']->getFlashBag()->add('success', 'User has been successfully added to the database.');
		}
		return $app['twig']->render('user_form.html.twig', array(
			'title' => 'New user',
			'userForm' => $userForm->createView()));
	}

	/**
	 * Admin delete user controller
	 * @param Application $app     Silex application
	 * @param  integer $id User id
	 * @return redirection 		HTML view of the admin page
	 */
	public function deleteUserAction(Application $app, $id) {
		$app['dao.link']->deleteAllByUser($id);
		$app['dao.user']->delete($id);
		$app['session']->getFlashBag()->add('success', 'The user and all his links have been successfully deleted.');
		return $app->redirect('/admin');
	}

	/**
	 * Admin edit link form page controller
	 * @param Application $app     Silex application
	 * @param Request     $request Incoming request
	 * @param  integer $id link id
	 * @return view             HTML view of the admin page
	 */
	public function editLinkAction(Application $app, Request $request, $id) {
		$link = $app['dao.link']->find($id);
		$linkForm = $app['form.factory']->create(new LinkType(), $link);
		$linkForm->handleRequest($request);
		if($linkForm->isSubmitted() && $linkForm->isValid()){
			$app['dao.link']->save($link);
			$app['session']->getFlashBag()->add('success', 'The link has successfully been updated.');
		}
		return $app['twig']->render('link_form.html.twig', array(
			'title' => 'Edit link',
			'linkForm' => $linkForm->createView()));
	}

	/**
	 * Admin delete link controller
	 * @param Application $app     Silex application
	 * @param  integer $id link id
	 * @return redirection 		HTML view of the admin page
	 */
	public function deleteLinkAction(Application $app, $id) {
		$app['dao.link']->delete($id);
		$app['session']->getFlashBag()->add('success', 'The link has been successfully deleted.');
		return $app->redirect('/admin');
	}
}
