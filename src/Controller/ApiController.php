<?php

namespace WebLinks\Controller;

use Silex\Application;
use WebLinks\Domain\Link;

class ApiController {

	/**
	 * API get the lists of all links
	 * @param  Application $app Silex Application
	 * @return All links in JSON format
	 */
	public function getLinksAction(Application $app) {
		$links = $app['dao.link']->findAll();

		$responseData = array();
		foreach ($links as $link) {
			$responseData[] = $this->buildLinkArray($link);
		}

		return $app->json($responseData);
	}

	/**
	 * API get the details of one link
	 * @param  Application $app Silex application
	 * @param  Integer     $id  The identifier of the link details are wished
	 * @return Link details in JSON format
	 */
	public function getLinkAction(Application $app, $id) {
		$link = $app['dao.link']->find($id);

		return $app->json($this->buildLinkArray($link));
	}

	/**
	 * Converts a link to an array for JSON encoding
	 * @param  Link   $link The link to transform to an array
	 * @return Array with all the data contained in a link ready for JSON encoding
	 */
	public function buildLinkArray(Link $link) {
		$user = $link->getUser();
		return array(
			'id' => $link->getId(),
			'title' => $link->getTitle(),
			'url' => $link->getUrl(),
			'user' => array(
				'id' => $user->getId(),
				'name' => $user->getUsername())
			);
	}
}
