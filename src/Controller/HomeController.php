<?php

namespace WebLinks\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use WebLinks\Domain\Link;
use WebLinks\Form\Type\LinkType;

class HomeController {
	/**
	 * Home page controller
	 * @param  Application $app Silex application
	 * @return view          HTML view for the home page
	 */
	public function indexAction(Application $app) {
		$links = $app['dao.link']->findAll();
    	return $app['twig']->render('index.html.twig', array('links' => $links));
	}

	/**
	 * Login page controller
	 * @param  Request     $request incoming request
	 * @param  Application $app     Silex application
	 * @return view              	HTML view for the login form
	 */
	public function loginAction(Request $request, Application $app) {
		return $app['twig']->render('login.html.twig', array(
			'error' => $app['security.last_error']($request),
			'last_username' => $app['session']->get('_security.last_username'),
			));
	}

	/**
	 * Link submission controller
	 * @param  Request     $request incoming request
	 * @param  Application $app     Silex application
	 * @return view              	HTML view for the link submission form
	 */
	public function submitlinkAction(Request $request, Application $app) {
		$link = new Link();
		$linkForm = $app['form.factory']->create(new LinkType(), $link);
		$linkForm->handleRequest($request);
		if($linkForm->isSubmitted() && $linkForm->isValid()) {
			$link->setUser($app['security']->getToken()->getUser());
			$app['dao.link']->save($link);
			$app['session']->getFlashBag()->add('success', 'The link was successfully created.');
		}
		return $app['twig']->render('link_form.html.twig', array(
			'title' => 'New URL',
			'linkForm' => $linkForm->createView()));
	}
}
