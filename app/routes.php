<?php

// Home page
$app->get('/', 'WebLinks\Controller\HomeController::indexAction');

// Login page
$app->get('/login', 'WebLinks\Controller\HomeController::loginAction')->bind('login');

// Link pages
$app->match('/link/submit', 'WebLinks\Controller\HomeController::submitlinkAction');

// Basic admin page
$app->get('/admin', 'WebLinks\Controller\AdminController::indexAction');

// Admin user pages
$app->match('/admin/user/add', 'WebLinks\Controller\AdminController::addUserAction');

$app->match('/admin/user/{id}/edit', 'WebLinks\Controller\AdminController::editUserAction');

$app->get('/admin/user/{id}/delete', 'WebLinks\Controller\AdminController::deleteUserAction');

// Admin link pages
$app->match('/admin/link/{id}/edit', 'WebLinks\Controller\AdminController::editLinkAction');

$app->get('/admin/link/{id}/delete', 'WebLinks\Controller\AdminController::deleteLinkAction');

// API links
$app->get('/api/links', 'WebLinks\Controller\ApiController::getLinksAction');
$app->get('/api/link/{id}', 'WebLinks\Controller\ApiController::getLinkAction');
